import styled from 'styled-components';
import background from '../../static/background.svg'


export const Background = styled.div`
    width: 100%;
    height: 100vh;
    background-image: url(${background});
    background-size: cover;
`;
