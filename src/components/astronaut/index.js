import React, { useState } from 'react';
import astronaut from '../../static/astronaut.svg';
import { AnsButton, AstronautContainer, ButtonsContainer } from './styled';
import check from '../../static/check.svg';
import cross from '../../static/cross.svg';


export const Astronaut = ({ tasks, onFinish }) => {
    const [taskIndex, setTaskIndex] = useState(0);
    const [lastChose, setChose] = useState();
    const [error, setError] = useState(false);

    const onAns = (ans) => {
        setChose(ans);
        if (tasks[taskIndex].ans === ans) {
            setTaskIndex(taskIndex + 1);
            playNext();

            if (taskIndex + 1 === tasks.length) {
                onFinish();
            }
        } else {
            setError(true);
            setTimeout(() => setError(false), 500);
        }
    }

    const playNext = () => {
        // playAudio(tasks[taskIndex].audio)
        // stopAudio(tasks[taskIndex - 1].audio)
    }

    return (
        <AstronautContainer>
            <img style={{marginBottom: '32px'}} src={astronaut} />
            <ButtonsContainer>
                <AnsButton onClick={() => onAns(true)} error={lastChose && error}>
                    <img src={check} />
                </AnsButton>
                <AnsButton onClick={() => onAns(false)} error={!lastChose && error}>
                    <img src={cross} />
                </AnsButton>
            </ButtonsContainer>
        </AstronautContainer>
    )
}
