import styled from 'styled-components';


export const AstronautContainer = styled.div`
    margin-top: 120px;
    display: flex;
    flex-direction: column;
    align-items: center;
`;

export const AnsButton = styled.button`
width: 64px;
height: 64px;background: #FFFFFF;
border-radius: 8px;
border: none;
cursor: pointer;
transition-duration: .3s;
    ${({ error }) => error && 'background: #EB5757;'}
`;

export const ButtonsContainer = styled.div`
    width: 160px;
    display: flex;
    justify-content: space-between;
`;
