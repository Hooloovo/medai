import styled from 'styled-components';


export const Text = styled.span`
    font-weight: bold;
    color: ${props => props.color ? props.color : '#FFF'}
`;

export const Text18 = styled(Text)`
    font-size: 18px;
    line-height: 25px;
`;

export const Text24 = styled(Text)`
    font-size: 24px;
    line-height: 33px;
`;

export const Text40 = styled(Text)`
    font-size: 40px;
    line-height: 55px;
`;
