import React, { useEffect, useState } from 'react';
import { FlexContainer } from '../container/styled';
import { FaceContainer, FaceContainers, Title } from './styled';
import mymodel from './model/facemo.json';


export const FaceCompare = () => {
    const [emotion, setEmotion] = useState();
    let { tf, faceLandmarksDetection } = window;

    async function setupWebcam() {
        return new Promise( ( resolve, reject ) => {
            const webcamElement = document.getElementById( "webcam" );
            const navigatorAny = navigator;
            navigator.getUserMedia = navigator.getUserMedia ||
            navigatorAny.webkitGetUserMedia || navigatorAny.mozGetUserMedia ||
            navigatorAny.msGetUserMedia;
            if( navigator.getUserMedia ) {
                navigator.getUserMedia( { video: true },
                    stream => {
                        webcamElement.srcObject = stream;
                        webcamElement.addEventListener( "loadeddata", resolve, false );
                    },
                error => reject());
            }
            else {
                reject();
            }
        });
    }

    async function trackFace() {
        const video = document.querySelector( "video" );
        const faces = await model.estimateFaces( {
            input: video,
            returnTensors: false,
            flipHorizontal: false,
        });
        output.drawImage(
            video,
            0, 0, video.width, video.height,
            0, 0, video.width, video.height
        );

        let points = null;
        faces.forEach( face => {
            // Draw the bounding box
            const x1 = face.boundingBox.topLeft[ 0 ];
            const y1 = face.boundingBox.topLeft[ 1 ];
            const x2 = face.boundingBox.bottomRight[ 0 ];
            const y2 = face.boundingBox.bottomRight[ 1 ];
            const bWidth = x2 - x1;
            const bHeight = y2 - y1;

            // Add just the nose, cheeks, eyes, eyebrows & mouth
            const features = [
                "noseTip",
                "leftCheek",
                "rightCheek",
                "leftEyeLower1", "leftEyeUpper1",
                "rightEyeLower1", "rightEyeUpper1",
                "leftEyebrowLower", //"leftEyebrowUpper",
                "rightEyebrowLower", //"rightEyebrowUpper",
                "lipsLowerInner", //"lipsLowerOuter",
                "lipsUpperInner", //"lipsUpperOuter",
            ];
            points = [];
            features.forEach( feature => {
                face.annotations[ feature ].forEach( x => {
                    points.push( ( x[ 0 ] - x1 ) / bWidth );
                    points.push( ( x[ 1 ] - y1 ) / bHeight );
                });
            });
        });

        if( points ) {
            let emotion = await predictEmotion( points );
            setEmotion( `Detected: ${emotion}` );
        }
        else {
            setEmotion( "No Face" );
        }

        requestAnimationFrame( trackFace );
    }

    const emotions = [ "angry", "disgust", "fear", "happy", "neutral", "sad", "surprise" ];
    let emotionModel = null;

    let output = null;
    let model = null;

    async function predictEmotion( points ) {
        let result = tf.tidy( () => {
            const xs = tf.stack( [ tf.tensor1d( points ) ] );
            return emotionModel.predict( xs );
        });
        let prediction = await result.data();
        result.dispose();
        // Get the index of the maximum value
        let id = prediction.indexOf( Math.max( ...prediction ) );
        return emotions[ id ];
    }


    ///

    useEffect(() => {
        async function init() {
            await setupWebcam();
            const video = document.getElementById( "webcam" );
            video.play();
            let videoWidth = video.videoWidth;
            let videoHeight = video.videoHeight;
            video.width = videoWidth;
            video.height = videoHeight;

            let canvas = document.getElementById( "output" );
            canvas.width = video.width;
            canvas.height = video.height;

            output = canvas.getContext( "2d" );
            output.translate( canvas.width, 0 );
            output.scale( -1, 1 ); // Mirror cam
            output.fillStyle = "#fdffb6";
            output.strokeStyle = "#fdffb6";
            output.lineWidth = 2;

            // Load Face Landmarks Detection
            model = await faceLandmarksDetection.load(
                faceLandmarksDetection.SupportedPackages.mediapipeFacemesh
            );
            // Load Emotion Detection
            emotionModel = await tf.loadLayersModel(tf.io.fileSystem("./model/facemo.json"));

            setEmotion( "Loaded!" );

            trackFace();
        }

        init();
    })

    ///

    return (
        <FlexContainer>
            <Title>Повтори за космонавтом звук А</Title>
            <FaceContainers>
                <FaceContainer></FaceContainer>
                <FaceContainer>
                <canvas id="output"></canvas>
                <video id="webcam" playsinline style={{
                    visibility: 'hidden',
                    width: 'auto',
                    height: 'auto',
                }}>
                </video>
                {emotion}
                </FaceContainer>
            </FaceContainers>
        </FlexContainer>
    )
}
