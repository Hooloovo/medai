import styled from 'styled-components';
import { Text40 } from '../palette/styled';


export const Title = styled(Text40)`
    margin-top: 67px;
    margin-bottom: 40px;
`;

export const FaceContainers = styled.div`
    width: 824px;
    height: 350px;
    display: flex;
    justify-content: space-between;
`;

export const FaceContainer = styled.div`
    width: 350px;
    height: 350px;
`
