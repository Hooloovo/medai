import React, { useState } from 'react';
import AudioAnalyser from "react-audio-analyser"
import { sendAudio } from '../../api';
import { ButtonStyled } from '../button/styled';


export const AudioRecorder = ({ time, onEnd, children }) => {
    const [isRecording, setStatus] = useState(false);

    const onStartRecord = () => {
        setTimeout(() => setStatus(false), time);
        setStatus(true);
    }
    const onStopRecord = (e) => {
        console.log(e);
        sendAudio(e).then((res) => console.log(res.data));
        onEnd(window.URL.createObjectURL(e), e);
    };
    return (
        <div>
            <div style={{display: isRecording ? 'block' : 'none'}}>
                <AudioAnalyser
                    audioType="audio/wav"
                    stopCallback={onStopRecord}
                    status={isRecording ? 'recording' : 'inactive'}
                    backgroundColor={'rgba(0, 0, 0, 0)'}
                />
            </div>
            {!isRecording && (
                <ButtonStyled
                    withMicrophone
                    onClick={() => onStartRecord()}
                >{children}</ButtonStyled>
            )}
        </div>
    )
}
