import { ButtonStyled } from "./styled";

export const Button = ({ children, ...rest }) => <ButtonStyled props={rest}>{children}</ButtonStyled>