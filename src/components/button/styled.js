import styled from 'styled-components';
import microphone from '../../static/microphone.svg';


export const ButtonStyled = styled.button`
    background: #2F80ED;
    border-radius: 8px;
    border: none;
    display: flex;
    align-items: center;
    padding: 7px 24px;
    cursor: pointer;
    color: #FFF;
    
    ${props => props.small ? `
        font-size: 18px;
        line-height: 25px;
    ` : `
        font-size: 24px;
        line-height: 33px;
    `}

    &:active {
        background: #1D6FDD;
    }

    ${props => props.withMicrophone && `
        &:before {
            content: url('${microphone}');
            margin-right: 16px;
        }
    `}
`;
