import styled from 'styled-components';


export const ActionContainer = styled.div`
    width: 100%;
    max-width: 831px;
    background: #FFFFFF;
    border-radius: 16px;
    padding: 25px 30px;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-wrap: wrap;
`;
