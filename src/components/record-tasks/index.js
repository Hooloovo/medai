import React, { useState } from 'react'
import { ActionContainer } from '../action-container/styled';
import { AudioRecorder } from '../audioRecorder';
import { FlexContainer } from '../container/styled';
import { ErrorAlien } from '../error-alien';
import { Title } from '../face-compare/styled';
import { Microphone } from '../microphone/styled';
import { Text40 } from '../palette/styled';


var task = [
    {
        current: 'один, два, три',

    }
]

export const RecordTasks = ({ title, tasks, onEnd }) => {
    const [currentItem, setItem] = useState(0);
    const [error, setError] = useState(false);

    const onComplete = () => {
        if (currentItem + 1 === tasks.length) {
            onEnd();
        }

        setItem(currentItem + 1);

        //if (request) {
        //     setItem(currentItem + 1);
        // } else {
        //     setError(true);
        // }
    }
    return (
        <>
            <ErrorAlien
                show={error}
                onClick={() => setError(false)}
            />
            <FlexContainer>
                <Title>{title}</Title>
                <ActionContainer style={{ marginBottom: '165px' }}>
                    <Microphone />
                    <Text40 color="#2F80ED">{tasks[currentItem].toString()}</Text40>
                </ActionContainer>
                <AudioRecorder
                    time={4000}
                    onEnd={(src, blob) => {
                        onComplete(src, blob);
                    }}
                >Начать запись</AudioRecorder>
            </FlexContainer>
        </>
    )
}
