import styled from 'styled-components';
import alien from '../../static/error-alien.svg';


export const Container = styled.div`
    width: 345px;
    height: 179px;
    background-image: url('${alien}');
    position: fixed;
    bottom: 0;
    left: 0;

    display: ${props => props.show ? 'block' : 'none'};
`;

export const ButtonContainer = styled.div`
    position: absolute;
    bottom: 77px;
    right: 59px;
`;
