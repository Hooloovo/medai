import React from 'react';
import { Button } from '../button/index';
import { ButtonContainer, Container } from './styled';


export const ErrorAlien = ({ show, onClick }) => {
    return (
        <Container show={show}>
            <ButtonContainer>
                <Button small onClick={onClick}>Хорошо</Button>
            </ButtonContainer>
        </Container>
    )
}
