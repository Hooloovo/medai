import React from 'react'
import { Microphone } from '../microphone/styled'
import { CardContainer } from './styled'


export const Card = ({ image, sound }) => {
    return (
        <CardContainer>
            <img src={image} />
            <Microphone style={{margin: '0'}} />
        </CardContainer>
    )
}
