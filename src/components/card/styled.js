import styled from 'styled-components';


export const CardContainer = styled.div`
    width: 250px;
    height: 300px;
    background: #FFFFFF;
    border-radius: 16px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-between;
    padding: 60px 0 24px 0;
`;
