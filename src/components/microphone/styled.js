import styled from 'styled-components';
import dynamic from '../../static/dynamic.svg';


export const Microphone = styled.button`
    width: 40px;
    height: 40px;
    border-radius: 20px;
    border: none;
    background: #2F80ED;
    display: flex;
    align-items: center;
    justify-content: center;
    content: url('${dynamic}');
    margin-right: 16px;
    cursor: pointer;

    &:active {
        background: #1D6FDD;
    }
`;
