import axios from 'axios';


export const sendAudio = (audio) => {
    var bodyFormData = new FormData();
    console.log(audio);
    bodyFormData.append('file', audio); 

    return axios({
        method: "post",
        url: "https://9c03-5-166-33-72.ngrok.io/upload",
        data: bodyFormData,
        headers: { "Content-Type": "multipart/form-data" },
    })
}
