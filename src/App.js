import { Breave } from "./pages/breave/breave";
import { Loud } from "./pages/breave/loud";
import { CongratulationsPage } from "./pages/main/congratulations";
import { MenuPage } from "./pages/main/menu";
import { OnboardindPage } from "./pages/main/onboarding";
import { Meeting } from "./pages/meeting";
import {
  BrowserRouter,
  Routes,
  Route
} from "react-router-dom";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<MenuPage />} />
        <Route path="/breave" element={<Breave />} />
        <Route path="/meet" element={<Meeting />} />
        <Route path="/loud" element={<Loud />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
