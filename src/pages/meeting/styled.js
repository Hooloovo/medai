import styled from 'styled-components';
import { Text24, Text40 } from '../../components/palette/styled';


export const Title = styled(Text40)`
    margin-top: 39px;
    margin-bottom: 91px;
`;

export const BigA = styled.span`
    line-height: 153px;
    font-size: 153px;
    color: #fff;
    margin-bottom: 20px;
`;

export const Rocket = styled.div`
    width: ${({ width }) => width};
    display: flex;
    flex-direction: column;
    align-items: center;

    ${({ hidden }) => !hidden ? 'opacity: 0;' : null}
`;

export const RocketImage = styled.img`
    width: 100%;
    object-fit: cover;
    margin-bottom: 8px;
`;

export const RocketText = styled(Text24)`
    text-align: center;
`;

export const RepeatContainer = styled.div`
    display: inline-flex;
    align-items: center;
    margin-top: 152px;
`;

export const RepeatActionContainer = styled.div`
    margin-left: 137px;
    display: flex;
    flex-direction: column;

    & > span {
        margin-bottom: 32px;
        margin-bottom: 16px;
    }
`;

export const CompareContainer = styled.div`
    width: 620px;
    display: flex;
    justify-content: space-between;
    margin-bottom: 32px;
`;

export const CardContainer = styled.div`
    width: 250px;
    height: 300px;
    background: #FFFFFF;
    border-radius: 16px;
    margin-bottom: 52px;
`;
