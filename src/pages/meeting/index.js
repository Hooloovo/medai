import React, { useState, useEffect } from 'react';
import { Background } from '../../components/background/styled';
import { Card } from '../../components/card/index';
import { Rocket, RocketContainer, RocketImage, Title, RocketText, RepeatContainer, RepeatActionContainer, BigA, CompareContainer, CardContainer } from './styled';
import rocket from '../../static/rocket.svg';
import { ButtonStyled } from '../../components/button/styled';
import { FlexContainer } from '../../components/container/styled';
import { Text40 } from '../../components/palette/styled';
import { Astronaut } from '../../components/astronaut';
import { ActionContainer } from '../../components/action-container/styled';
import { Microphone } from '../../components/microphone/styled';
import { FaceCompare } from '../../components/face-compare';
import AudioAnalyser from "react-audio-analyser"
import { AudioRecorder } from '../../components/audioRecorder';
import { RecordTasks } from '../../components/record-tasks';
import { Congratulations } from '../main/congratulations';


export const Meeting = () => {
    const [stage, setStage] = useState(1);
    const [compareIndex, setIndex] = useState(0);
    const [isRecording, setStatus] = useState(false);
    const [audioSrc, setSrc] = useState();
    const [audioBlob, setBlob] = useState();

    const compareItems = [
        [{
            image: rocket,
            sound: {},
        },
        {
            image: rocket,
            sound: {},
        },],
        [{
            image: rocket,
            sound: {},
        },
        {
            image: rocket,
            sound: {},
        },],
    ];

    const onStartRecord = () => {
        setTimeout(() => setStatus(false), 1000);
        setStatus(true);
    }
    const onStopRecord = (e) => {
        setSrc(window.URL.createObjectURL(e));
        setBlob(e);
        console.log(window.URL.createObjectURL(e), e)
    }
    return (
        <Background>
            {stage === 1 && <FlexContainer>
                <Title>Познакомься, это звук A</Title>
                <BigA>A</BigA>
                <ButtonStyled style={{marginBottom: '74px'}}>Послушать </ButtonStyled>
                <ButtonStyled onClick={() => setStage(2)}>Дальше</ButtonStyled>
            </FlexContainer>}
            {stage === 2 && (
                <FlexContainer>
                    <Title>В каком слове есть звук А?</Title>
                    <CompareContainer>
                        <Card
                            image={compareItems[compareIndex][0].image}
                            sound={compareItems[compareIndex][0].sound}
                        />
                        <Card
                            image={compareItems[compareIndex][1].image}
                            sound={compareItems[compareIndex][1].sound}
                        />
                    </CompareContainer>
                    <ButtonStyled onClick={() => setStage(3)}>Дальше</ButtonStyled>
                </FlexContainer>
            )}
            {stage === 3 && (
                <FlexContainer>
                    <Astronaut
                        tasks={[{
                            ans: true,
                            audio: {},
                        }]}
                        onFinish={() => setStage(4)}
                    />
                </FlexContainer>
            )}
            {stage === 4 && (
                <FlexContainer>
                    <Title>Чтобы произнести звук А нужно:</Title>
                    <CardContainer></CardContainer>
                    <ButtonStyled onClick={() => setStage(6)}>Дальше</ButtonStyled>
                </FlexContainer>
            )}
            {stage === 5 && (
                <FaceCompare />
            )}
            {stage === 6 && (
                <RecordTasks
                    tasks={['ПА', 'ТА', 'MА', 'ЛА', 'ДА']}
                    title="Повтори:"
                    onEnd={() => setStage(7)}
                />
            )}
            {stage === 7 && (
                <Congratulations handleClick={() => setStage(8)} />
            )}
            {stage === 8 && (
                <RecordTasks
                    tasks={['САМ', 'ТАМ', 'МАМ', 'ДАЛ', 'ПАП', 'СА-МА']}
                    title="Повтори:"
                    onEnd={() => setStage(10)}
                />
            )}
            {stage === 9 && (
                <Congratulations handleClick={() => setStage(10)} />
            )}
            {stage === 10 && (
                <RecordTasks
                    tasks={['СА-МА', 'МА-МА', 'ПА-ПА', 'ДА-ЛА']}
                    title="Повтори:"
                    onEnd={() => setStage(11)}
                />
            )}
            {stage ===11 && (
                <Congratulations handleClick={() => setStage(12)} />
            )}
            {stage === 12 && (
                <RecordTasks
                    tasks={['Я сегодня утром рано, Умывался из – под ...', 'Для птенцов и для зверят, Тоже нужен детский ...', 'Не послушал Зайка папу, Отдавили Зайке ...', 'Солнце светит слишком ярко,Бегемоту стало ...']}
                    title="Повтори:"
                    onEnd={() => setStage(13)}
                />
            )}
            {stage ===13 && (
                <Congratulations handleClick={() => ({})} />
            )}
        </Background>
    );
}
