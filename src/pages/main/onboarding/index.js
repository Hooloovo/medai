import React from 'react';
import { Background } from '../../../components/background/styled';
import { ActionContainer } from '../../../components/action-container/styled';
import { Text40 } from '../../../components/palette/styled';
import { FlexContainer } from '../../../components/container/styled';
import { Container } from './styled';



export const OnboardindPage = () => {
    return (
        <Background>
            <FlexContainer>
                <Container>
                    <ActionContainer>
                        <Text40 color="#2F80ED">Онбординг</Text40>
                    </ActionContainer>
                </Container>
            </FlexContainer>
        </Background>
    )
}
