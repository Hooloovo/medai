import styled from 'styled-components';
import planets from '../../../static/planets.svg'


export const Planets = styled.div`
    width: 1024px;
    height: 560px;
    position: absolute;
    left: calc(50vw - 50%);
    top: 0;
    background-image: url('${planets}');
`;

export const PlanetButton = styled.button`
    cursor: pointer;
    border: none;
    background: transparent;
    content: 'A';
    font-size: 72px;
    color: rgba(255, 255, 255, .7);
    position: absolute;
    bottom: 105px;
    left: 165px;
    transition-duration: .3s;

    &:hover {
        color: rgb(255, 255, 255);
    }
`;
