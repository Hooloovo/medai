import React from 'react';
import { Background } from '../../../components/background/styled';
import { PlanetButton, Planets } from './styled';
import { Link } from "react-router-dom";


export const MenuPage = () => {
    return (
        <Background>
            <Planets>
                <Link to="/breave"><PlanetButton>A</PlanetButton></Link>
            </Planets>
        </Background>
    )
}
