import styled from 'styled-components';
import { FlexContainer } from '../../../components/container/styled';


export const Container = styled(FlexContainer)`
    padding-top: 80px;
`;

export const AlienContainer = styled.img`
    width: 590px;
    margin-bottom: 60px;
`;
