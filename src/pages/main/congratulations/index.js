import React from 'react'
import { Background } from '../../../components/background/styled'
import { AlienContainer, Container } from './styled'
import starship from '../../../static/starship.svg';
import { Button } from '../../../components/button/index';
import { ErrorAlien } from '../../../components/error-alien';
import { ButtonStyled } from '../../../components/button/styled';


export const Congratulations = ({ handleClick }) => {
    return (
        <Container>
            <AlienContainer src={starship} />
            <ButtonStyled onClick={() => handleClick()}>Следующее задание</ButtonStyled>
        </Container>
    )
}
