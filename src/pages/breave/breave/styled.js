import styled from 'styled-components';
import { ActionContainer } from '../../../components/action-container/styled';
import { FlexContainer } from '../../../components/container/styled';
import { Text40 } from '../../../components/palette/styled';


export const Container = styled(FlexContainer)`
    padding-top: 60px;
`;

export const Title = styled(Text40)`
    margin-bottom: 57px;
`;

export const ActionContainerStyled = styled(ActionContainer)`
    margin-bottom: 165px;
`;
