import React, { useState } from 'react';
import { Background } from '../../../components/background/styled';
import { Microphone } from '../../../components/microphone/styled';
import { Container, Title, ActionContainerStyled } from './styled';
import { Text40 } from '../../../components/palette/styled';
import { Button } from '../../../components/button';
import { ButtonStyled } from '../../../components/button/styled';
import { RecordTasks } from '../../../components/record-tasks';
import { Congratulations } from '../../main/congratulations';


export const Breave = () => {
    const [stage, setStage] = useState(0);
    return (
        <Background>
            <Container>
                {stage === 0 && <RecordTasks
                    tasks={['один', 'один, два, три', 'один, два, три, четыре, пять']}
                    title="Глубоко вдохни и на выдохе прочитай:"
                    onEnd={() => setStage(1)}
                />}
                {stage === 1 && <Congratulations handleClick={() => setStage(2)} />}
                {stage === 2 && <RecordTasks
                    tasks={['один, два, три, четыре, пять, шесть, семь, восемь', 'один, два, три, четыре, пять, шесть, семь, восемь, девять, десять']}
                    title="Глубоко вдохни и на выдохе прочитай:"
                    onEnd={() => setStage(3)}
                />}
                {stage === 3 && <Congratulations handleClick={() => window.location.href="/loud"} />}
            </Container>
        </Background>
    )
}
