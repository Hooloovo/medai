import styled from 'styled-components';
import { Text24, Text40 } from '../../../components/palette/styled';


export const Title = styled(Text40)`
    margin-top: 39px;
    margin-bottom: 20px;
`;

export const RocketContainer = styled.div`
    max-width: 850px;
    width: 100%;
    display: flex;
    align-items: flex-end;
    justify-content: space-between;
    margin-bottom: 29px;
`;

export const Rocket = styled.div`
    width: ${({ width }) => width};
    display: flex;
    flex-direction: column;
    align-items: center;

    ${({ hidden }) => !hidden ? 'opacity: 0;' : null}
`;

export const RocketImage = styled.img`
    width: 100%;
    object-fit: cover;
    margin-bottom: 8px;
`;

export const RocketText = styled(Text24)`
    text-align: center;
`;

export const RepeatContainer = styled.div`
    display: inline-flex;
    align-items: center;
    margin-top: 152px;
`;

export const RepeatActionContainer = styled.div`
    margin-left: 137px;
    display: flex;
    flex-direction: column;

    & > span {
        margin-bottom: 32px;
        margin-bottom: 16px;
    }
`;
