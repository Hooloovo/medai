import React, { useState, useEffect } from 'react';
import { Background } from '../../../components/background/styled';
import { Rocket, RocketContainer, RocketImage, Title, RocketText, RepeatContainer, RepeatActionContainer } from './styled';
import rocket from '../../../static/rocket.svg';
import { ButtonStyled } from '../../../components/button/styled';
import { FlexContainer } from '../../../components/container/styled';
import { Text40 } from '../../../components/palette/styled';
import { AudioRecorder } from '../../../components/audioRecorder';


export const Loud = () => {
    const [isAction, setAction] = useState(false);
    const [timeouts, setTimeouts] = useState(undefined);
    const [stage, setStage] = useState(1);
    const [currentRocket, setCurrent] = useState(0);

    const rockets = [
        {
            title: 'Тихая ракета',
            width: '80px'
        },
        {
            title: 'Громкая ракета',
            width: '120px'
        },
        {
            title: 'Самая громкая ракета',
            width: '140px'
        },
    ]

    const play = async () => {
        let id, n;
        n = 1;
        const timeFunc = () => {
            if (n <= 3) {
                setStage(n);
                id = setTimeout(() => timeFunc(), 1000);
                setTimeouts(id);
                n++;
            } else {
                setAction(true);
            }
        }
        timeFunc();
        setTimeouts(id)
    }

    const repeat = () => {
        clearTimeout(timeouts)
        setStage(1);
        play();
    }

    useEffect(() => {
        play();
    }, []);

    const onNext = () => {
        if (currentRocket + 1 === 3) {
            window.location.href = '/meet'
        }
        setCurrent(currentRocket + 1)
    }
    return (
        <Background>
            {!isAction ? (
                <FlexContainer>
                    <Title>Ракеты имеют разную громкость. Послушай!</Title>
                    <RocketContainer>
                        <Rocket width="80px" hidden={stage > 0}>
                            <RocketImage src={rocket} />
                            <RocketText>Тихая ракета</RocketText>
                        </Rocket>
                        <Rocket width="120px" hidden={stage > 1}>
                            <RocketImage src={rocket} />
                            <RocketText>Громкая ракета</RocketText>
                        </Rocket>
                        <Rocket width="140px" hidden={stage > 2}>
                            <RocketImage src={rocket} />
                            <RocketText>Самая громкая ракета</RocketText>
                        </Rocket>
                    </RocketContainer>
                    <ButtonStyled onClick={repeat}>Послушать еще раз</ButtonStyled>
                </FlexContainer>
            ) : (
                <FlexContainer>
                    <RepeatContainer>
                        <Rocket width={rockets[currentRocket].width} hidden={true}>
                            <RocketImage src={rocket} />
                            <RocketText>{rockets[currentRocket].title}</RocketText>
                            <ButtonStyled style={{marginTop: '16px'}} small>Послушать</ButtonStyled>
                        </Rocket>
                        <RepeatActionContainer>
                            <Text40>Повтори звук за ракетой</Text40>
                            <AudioRecorder time={3000} onEnd={() => onNext()}>
                                Начать запись
                            </AudioRecorder>
                        </RepeatActionContainer>
                    </RepeatContainer>
                </FlexContainer>
            )}
        </Background>
    );
}
