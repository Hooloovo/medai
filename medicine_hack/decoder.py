import numpy as np
import math
import itertools
from scipy.special import softmax
np.seterr(divide='ignore')


class DecodeResult:
    def __init__(self, score, words):
        self.score, self.words = score, words
        self.text = " ".join(word["word"] for word in words)


class GreedyDecoder:
    def __init__(self, labels, blank_idx=0):
        self.labels, self.blank_idx = labels, blank_idx
        self.delim_idx = self.labels.index(" ")


    def decode(self, output, start_timestamp=0, frame_time=0.02):
        best_path = np.argmax(output.astype(np.float32, copy=False), axis=1)
        score = None

        words, new_word, i = [], True, 0
        current_word, current_timestamp, end_idx = None, start_timestamp, 0
        words_len = 0

        for k, g in itertools.groupby(best_path):
            if k != self.blank_idx:
                if new_word and k != self.delim_idx:
                    new_word, start_idx = False, i
                    current_word, current_timestamp = self.labels[k], frame_time * i + start_timestamp

                elif k == self.delim_idx:
                    end_timestamp = frame_time * i + start_timestamp
                    new_word, end_idx = True, i
                    word_score = output[range(start_idx, end_idx), best_path[range(start_idx, end_idx)]] - np.max(output)
                    if score is not None:
                        score = np.hstack([score, word_score])
                    else:
                        score = word_score
                    word_confidence = np.round(np.exp(word_score.mean() / max(1, end_idx - start_idx)) * 100.0, 2)
                    words_len += end_idx - start_idx
                    words.append({
                        "word": current_word,
                        "start": np.round(current_timestamp, 2),
                        "end": np.round(end_timestamp, 2),
                        "confidence": word_confidence
                    })

                else:
                    current_word += self.labels[k]

            i += sum(1 for _ in g)

        # score = np.round(np.exp(score.mean() / max(1, words_len)) * 100.0, 2)
        score = 0

        return DecodeResult(score, words)