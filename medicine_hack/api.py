from flask import Flask
from flask import request
from flask import make_response
from uuid import uuid4
import speech_recognizer
from flask_cors import CORS

app = Flask(__name__)
# CORS(app)

@app.route('/upload', methods=['POST'])
def upload_file():
    if request.method == 'POST':
        f = request.files['file']
        _id = uuid4()
        f.save(f'./data/{_id}.wav')

        recognizer = speech_recognizer.SpeechRecognizer("config.ini")
        result = recognizer.recognize(f"./data/{_id}.wav")
        print(result.text)

        return result.text

